# reviews

My reviews of things and places.

## Tools

To retrieve the Google Maps Place ID of something, use [their Place ID service/example](https://developers.google.com/maps/documentation/places/web-service/place-id).

## Plans

I’ll convert these reviews to some kind of website maybe at some point.
For now, I’m just using it to paste the same reviews on different platforms.
